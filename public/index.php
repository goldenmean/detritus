<?php
namespace Aturner\Detritus;

define('ROOT_PATH', dirname(__DIR__));
require_once(ROOT_PATH . '/vendor/autoload.php');

try {
    $bootstrap   = new Bootstrap(ROOT_PATH);
    $application = $bootstrap->getApplication();
    $application->execute();
} catch (\Throwable $error) {
    echo $error->getMessage() . "\n";
    echo $error->getFile() . "\n";
    echo $error->getLine() . "\n";
}
