<?php
namespace Aturner\Detritus;

use Aturner\Detritus\Controllers\Base as Controller;
use Psr\Container\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class Application
 *
 * @package Aturner\Detritus
 * @copyright zZounds Music, LLC (c) 1996 - 2018 All rights reserved
 * @license MIT
 */
class Application
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var Controller
     */
    private $controller;

    /**
     * @var Request
     */
    private $request;

    /**
     * Application constructor
     *
     * @param ContainerInterface $container
     * @param Request $request
     */
    public function __construct(ContainerInterface $container, Request $request)
    {
        $this->container = $container;
        $this->request   = $request;
    }

    /**
     * Execute the program
     */
    public function execute(): void
    {
        try {
            $this->controller = $this->getController();
            $this->controller->execute($this->request);
        } catch (\Throwable $e) {
            echo $e->getMessage();
        }
    }

    /**
     * @return Controller
     */
    protected function getController(): Controller
    {
        $type = Controller::class;
        if (!$this->container->has($type)) {
            throw new \InvalidArgumentException(
                'Requested controller is not defined'
            );
        }

        return $this->container->get($type);
    }
}
