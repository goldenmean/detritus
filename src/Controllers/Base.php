<?php
namespace Aturner\Detritus\Controllers;

use Aturner\Detritus\Models\Forms\Base as Model;
use Aturner\Detritus\Views\Base as View;
use Symfony\Component\HttpFoundation\Request;

class Base
{
    /**
     * @var Model
     */
    private $model;

    /**
     * @var View
     */
    private $view;

    /**
     * Base Model constructor
     *
     * @param Model $model
     * @param View $view
     */
    public function __construct(Model $model, View $view)
    {
        $this->model = $model;
        $this->view  = $view;
    }

    /**
     * Execute the controller
     */
    public function execute(Request $request): void
    {
        $form = $this->model->buildForm();
        $form->handleRequest($request);
        $this->view->display('index.twig', ['form' => $form->createView()]);
    }
}
