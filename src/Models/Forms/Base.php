<?php
namespace Aturner\Detritus\Models\Forms;

use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class Base
{
    /**
     * @var FormFactoryInterface
     */
    private $factory;

    /**
     * Base constructor.
     * @param FormFactoryInterface $factory
     */
    public function __construct(FormFactoryInterface $factory)
    {
        $this->factory = $factory;
    }

    /**
     * @return FormInterface
     */
    public function buildForm(): FormInterface
    {
        return $this->factory->createBuilder(FormType::class, null, [
                'action' => '/index.php',
                'attr'   => ['novalidate' => 'novalidate']
            ])
            ->add('first_name', TextType::class, [
                'label'       => 'First Name',
                'required'    => true,
                'constraints' => [
                    new NotBlank(['message' => '** First name is required']),
                    new Length(['min' => 3, 'max' => 32])
                ]
            ])
            ->add('last_name', TextType::class, [
                'label'    => 'Last Name',
                'required' => true,
            ])
            ->add('quantity', NumberType::class, [
                'label'       => 'Quantity',
                'required'    => true,
                'constraints' => [
                    new NotBlank(),
                ],
                'invalid_message' => 'Foo bar, stupid!',
            ])
            ->add('comments', TextareaType::class, [
                'attr'  => ['rows' => 10],
                'label' => 'Comments',
            ])
            ->add('submit', SubmitType::class, [
                'attr'  => ['class' => 'text-uppercase btn-primary btn-block'],
                'label' => 'Click To Submit',
            ])
            ->getForm();
    }
}
