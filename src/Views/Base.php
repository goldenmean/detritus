<?php
namespace Aturner\Detritus\Views;

use Symfony\Bridge\Twig\Extension\FormExtension;
use Symfony\Bridge\Twig\Extension\TranslationExtension;
use Symfony\Bridge\Twig\Form\TwigRendererEngine;
use Symfony\Component\Form\FormRenderer;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;
use Symfony\Component\Translation\TranslatorInterface;
use Twig\RuntimeLoader\FactoryRuntimeLoader;

class Base
{
    /**
     * @var \Twig_Environment
     */
    protected $twig;

    public function __construct(
        \Twig_Environment $twig,
        TwigRendererEngine $engine,
        CsrfTokenManagerInterface $manager,
        TranslatorInterface $translator
    ) {
        $this->twig = $twig;
        $this->twig->addRuntimeLoader(new FactoryRuntimeLoader(
            [FormRenderer::class => function () use ($engine, $manager) {
                return new FormRenderer($engine, $manager);
            }]
        ));
        $this->twig->addExtension(new FormExtension());
        $this->twig->addExtension(new TranslationExtension($translator));
    }

    /**
     * @param string $template
     * @param array $params
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function display(string $template, array $params): void
    {
        echo $this->twig->render($template, $params);
    }
}
