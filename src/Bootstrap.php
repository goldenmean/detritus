<?php
namespace Aturner\Detritus;

use Psr\Container\ContainerInterface;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

/**
 * Class Bootstrap
 *
 * @package Aturner\Detritus
 * @copyright zZounds Music, LLC (c) 1996 - 2018 All rights reserved
 * @license MIT
 */
class Bootstrap
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var Application
     */
    private $application;

    /**
     * @var string The root path to the project
     */
    private $rootPath;

    /**
     * Bootstrap constructor
     *
     * @param string $path
     */
    public function __construct(string $path)
    {
        $this->rootPath = $path;
    }

    /**
     * @throws \Exception
     * @return ContainerInterface
     */
    public function getContainer(): ContainerInterface
    {
        if (!$this->container instanceof ContainerInterface) {
            $builder = new ContainerBuilder();
            $loader  = new YamlFileLoader(
                $builder,
                new FileLocator($this->rootPath)
            );
            $loader->load('config/services.yml');
            $builder->setParameter('root_path', $this->rootPath);
            $builder->setParameter('template_path', $this->rootPath . '/templates');
            $builder->setParameter(
                'twig_resources',
                'vendor/symfony/twig-bridge/Resources/views/Form'
            );
            $builder->setParameter(
                'twig_options',
                ['cache' => false]
            );
            $builder->compile();
            $this->container = $builder->get('service_container');
        }

        return $this->container;
    }

    /**
     * @throws \Exception
     * @return Application
     */
    public function getApplication(): Application
    {
        if (!$this->application instanceof Application) {
            $container         = $this->getContainer();
            $this->application = $container->get(Application::class);
        }

        return $this->application;
    }
}
