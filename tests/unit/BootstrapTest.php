<?php
namespace Aturner\Detritus;

use PHPUnit\Framework\TestCase;
use Psr\Container\ContainerInterface;

/**
 * Class BootstrapTest
 *
 * @package Aturner\Detritus
 * @coversDefaultClass \Aturner\Detritus\Bootstrap
 */
class BootstrapTest extends TestCase
{
    /**
     * @var string
     */
    protected static $rootPath = '';

    public static function setUpBeforeClass()
    {
        self::$rootPath = dirname(__DIR__, 2);
        parent::setUpBeforeClass();
    }

    /**
     * @throws \Exception
     * @covers ::getContainer
     */
    public function testGetContainer()
    {
        $unit = new Bootstrap(self::$rootPath);
        $this->assertInstanceOf(ContainerInterface::class, $unit->getContainer());
    }

    /**
     * @throws \Exception
     * @covers ::getApplication
     */
    public function testGetApplication()
    {
        $unit = new Bootstrap(self::$rootPath);
        $this->assertInstanceOf(Application::class, $unit->getApplication());
    }
}
