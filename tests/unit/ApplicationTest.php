<?php
namespace Aturner\Detritus;

use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class ApplicationTest
 *
 * @package Aturner\Detritus
 * @coversDefaultClass \Aturner\Detritus\Application
 */
class ApplicationTest extends TestCase
{
    /**
     * @var string
     */
    protected static $rootPath;

    /**
     * Set the root path for the framework
     */
    public static function setUpBeforeClass()
    {
        self::$rootPath = dirname(__DIR__, 2);
        parent::setUpBeforeClass();
    }

    /**
     * @covers ::execute
     * @throws \Exception
     */
    public function testExecute()
    {
        $request   = Request::create('');
        $container = (new Bootstrap(self::$rootPath))->getContainer();
        $unit      = new Application($container, $request);
        ob_start();
        $unit->execute();
        $output = ob_get_clean();
        $this->assertStringContainsString('<title>Detritus</title>', $output);
    }
}
